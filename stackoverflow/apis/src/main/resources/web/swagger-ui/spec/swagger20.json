{
  "swagger" : "2.0",
  "info" : {
    "description" : "An application to simulate basic StackOverflow behaviour, mainly for learning modern application development practices.",
    "version" : "1.0",
    "title" : "StackOverflow API emulation",
    "termsOfService" : "",
    "contact" : { }
  },
  "basePath" : "/apis",
  "consumes" : [ "application/json" ],
  "produces" : [ "application/json" ],
  "tags": [
    {
      "name": "user",
      "description": "User APIs"
    },
    {
      "name": "token",
      "description": "Token APIs"
    },
    {
      "name": "question",
      "description": "Question APIs"
    },
    {
      "name": "question-comment",
      "description": "Comments on questions"
    },
    {
      "name": "answer",
      "description": "Answer APIs"
    },
    {
      "name": "answer-comment",
      "description": "Comments on answers"
    }
  ],
  "paths" : {
    "/users" : {
      "get" : {
      	"tags": [
          "user"
        ],
        "summary" : "Search users",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "email",
          "in" : "query",
          "required" : true,
          "type" : "string",
          "description" : "email address of the user",
          "x-example" : "email=johndoe@test.com"
        }, {
          "name" : "page",
          "in" : "query",
          "required" : false,
          "type" : "integer",
          "description" : "The page number to return. This API is paginated. If left empty, defaults to 1",
          "x-example" : "page=1"
        }, {
          "name" : "size",
          "in" : "query",
          "required" : false,
          "type" : "integer",
          "description" : "The number of result instances to return. If left empty, defaults to 10",
          "x-example" : "size=10"
        }, {
          "name" : "firstName",
          "in" : "query",
          "required" : false,
          "type" : "string",
          "description" : "First name of user",
          "x-example" : "firstName=John"
        }, {
          "name" : "lastName",
          "in" : "query",
          "required" : false,
          "type" : "string",
          "description" : "Last name of user",
          "x-example" : "lastName=Doe"
        }, {
          "name" : "Content-Type",
          "in" : "header",
          "required" : true,
          "type" : "string",
          "description" : "application/json"
        } ],
        "responses" : {
          "200" : {
            "description" : "Status 200",
            "schema" : {
              "type" : "array",
              "description" : "List of matching questions along with answers and comments.",
              "items" : {
                "$ref" : "#/definitions/User"
              }
            }
          }
        }
      },
      "post" : {
      	"tags": [
          "user"
        ],
        "summary" : "Create user",
        "description" : "TODO : Find authorization mechanism.",
        "consumes" : [ "application/json" ],
        "parameters" : [ {
          "name" : "Content-Type",
          "in" : "header",
          "required" : false,
          "type" : "string",
          "description" : "application/json"
        }, {
          "name" : "body",
          "in" : "body",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/User"
          }
        } ],
        "responses" : {
          "201" : {
            "description" : "User created successfully",
            "headers" : {
              "Location" : {
                "type" : "string",
                "description" : "/users/{id}",
                "x-example" : "/users/4455-2212-3345-7765"
              }
            }
          },
          "401" : {
            "description" : "If user is not authenticated, due to missing or incorrect Authorization header."
          },
          "403" : {
            "description" : "If user is not authorized to create a new user."
          },
          "422" : {
            "description" : "The POST body did not contain all the required attributes."
          }
        }
      },
      "x-restlet" : {
        "section" : "User APIs"
      }
    },
    "/users/{id}" : {
      "get" : {
      	"tags": [
          "user"
        ],
        "summary" : "Get user by ID",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "Content-Type",
          "in" : "header",
          "required" : true,
          "type" : "string",
          "description" : "application/json"
        } ],
        "responses" : {
          "200" : {
            "description" : "Status 200",
            "schema" : {
              "$ref" : "#/definitions/User"
            }
          },
          "404" : {
            "description" : "The entity is not found."
          }
        }
      },
      "patch" : {
      	"tags": [
          "user"
        ],
        "summary" : "Update user",
        "consumes" : [ ],
        "parameters" : [ {
          "name" : "Content-Type",
          "in" : "header",
          "required" : true,
          "type" : "string",
          "description" : "application/json"
        }, {
          "name" : "Authorization",
          "in" : "header",
          "required" : true,
          "type" : "string",
          "description" : "Bearer asdf2364saf8796324xcihkj23h42oi34"
        }, {
          "name" : "body",
          "in" : "body",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/User"
          },
          "x-examples" : {
            "application/json" : "{firstName: \"John\", lastName: \"Doe\"\"}"
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "Updated successfully."
          },
          "401" : {
            "description" : "Missing or invalid authorization token."
          },
          "403" : {
            "description" : "If an user tries to update another user's entity."
          },
          "404" : {
            "description" : "The entity is not found."
          },
          "422" : {
            "description" : "If the JSON body does not contain the required attributes."
          }
        }
      },
      "parameters" : [ {
        "name" : "id",
        "in" : "path",
        "required" : true,
        "type" : "string"
      } ],
      "x-restlet" : {
        "section" : "User APIs"
      }
    },
    "/tokens" : {
      "post" : {
      	"tags": [
          "token"
        ],
        "summary" : "Generate token",
        "description" : "Currently, token generation is done based on user's email and password.\nIn future, we may integrate with OpenID based systems like Google or Facebook for authentication.\nNote, this POST method (essentially a *write operation*) does **not** require an Authorization request header.",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "Content-Type",
          "in" : "header",
          "required" : true,
          "type" : "string",
          "description" : "application/json"
        }, {
          "name" : "Accept",
          "in" : "header",
          "required" : true,
          "type" : "string",
          "description" : "application/json"
        }, {
          "name" : "body",
          "in" : "body",
          "required" : true,
          "schema" : {
            "type" : "object"
          },
          "x-examples" : {
            "application/json" : "{\n  email: \"johndoe@stackoverflow.com\",\n  password: \"secret\"\n}"
          }
        } ],
        "responses" : {
          "201" : {
            "description" : "Authentication successful.",
            "schema" : {
              "$ref" : "#/definitions/User"
            },
            "headers" : {
              "Authorization" : {
                "type" : "string",
                "description" : "The generated JWT token",
                "x-example" : "Authorization: 832saf876wq3rgqwe876324gware32sad"
              }
            }
          },
          "401" : {
            "description" : "If username / password is incorrect."
          }
        }
      },
      "x-restlet" : {
        "section" : "User APIs"
      }
    },
    "/questions" : {
      "get" : {
      	"tags": [
          "question"
        ],
        "summary" : "Search questions",
        "description" : "Search question collection by question text. The result is paginated. It will return only the first level JSON object. Comments and answers will not be returned.",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "text",
          "in" : "query",
          "required" : false,
          "type" : "string",
          "description" : "Question text to be searched, passed in URL encoded form. If empty, the complete collection is returned, with pagination.",
          "x-example" : "text=vertx%20%scaling"
        }, {
          "name" : "skip",
          "in" : "query",
          "required" : false,
          "type" : "integer",
          "description" : "This API is paginated. The number of records to skip. Defaults to 0",
          "x-example" : "skip=10"
        }, {
          "name" : "limit",
          "in" : "query",
          "required" : false,
          "type" : "integer",
          "description" : "The number of result instances to return. Defaults to 10",
          "x-example" : "limit=10"
        }, {
          "name" : "Content-Type",
          "in" : "header",
          "required" : true,
          "type" : "string",
          "description" : "application/json"
        } ],
        "responses" : {
          "200" : {
            "description" : "Status 200",
            "schema" : {
              "type" : "object",
              "description" : "List of matching questions along with answers and comments.",
              "properties" : {
                "total" : {
                  "type" : "integer",
                  "description" : "Total records found matching the search query."
                },
                "data" : {
                  "type" : "array",
                  "description" : "Array of question objects.",
                  "items" : {
                    "$ref" : "#/definitions/Question"
                  }
                }
              }
            }
          },
          "400" : {
            "description" : "If any of the input is invalid. Example: skip=\"hello\""
          }
        }
      },
      "post" : {
      	"tags": [
          "question"
        ],
        "summary" : "Post a question",
        "consumes" : [ "application/json" ],
        "parameters" : [ {
          "name" : "Content-Type",
          "in" : "header",
          "required" : false,
          "type" : "string",
          "description" : "application/json"
        }, {
          "name" : "Authorization",
          "in" : "header",
          "required" : true,
          "type" : "string",
          "description" : "Bearer asdf2364saf8796324xcihkj23h42oi34"
        }, {
          "name" : "body",
          "in" : "body",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/Question"
          }
        } ],
        "responses" : {
          "201" : {
            "description" : "Question posted successfully.",
            "headers" : {
              "Location" : {
                "type" : "string",
                "description" : "/questions/{id}",
                "x-example" : "/questions/4455-2212-3345-7765"
              }
            }
          },
          "401" : {
            "description" : "If user is not authenticated, due to missing or incorrect Authorization header."
          },
          "422" : {
            "description" : "The POST body did not contain all the required attributes."
          }
        }
      },
      "x-restlet" : {
        "section" : "Question APIs"
      }
    },
    "/questions/{id}" : {
      "get" : {
      	"tags": [
          "question"
        ],
        "summary" : "Get question by ID",
        "description" : "Get the Question document along with all comments, answers and comments for answers.",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "Content-Type",
          "in" : "header",
          "required" : true,
          "type" : "string",
          "description" : "application/json"
        } ],
        "responses" : {
          "200" : {
            "description" : "Status 200",
            "schema" : {
              "$ref" : "#/definitions/Question"
            }
          },
          "404" : {
            "description" : "The entity is not found."
          }
        }
      },
      "patch" : {
      	"tags": [
          "question"
        ],
        "summary" : "Update question",
        "description" : "Update a question entity:\n1. Update content.\n2. Vote Up.\n3. Vote Down.",
        "consumes" : [ ],
        "parameters" : [ {
          "name" : "action",
          "in" : "query",
          "required" : false,
          "type" : "string",
          "description" : "The type of update done.\n1. For vote up: ?action=VOTE_UP\n2. For vote down: ?action=VOTE_DOWN\n3. For content update: ?action=CONTENT_UPDATE"
        }, {
          "name" : "Content-Type",
          "in" : "header",
          "required" : true,
          "type" : "string",
          "description" : "application/json"
        }, {
          "name" : "Authorization",
          "in" : "header",
          "required" : true,
          "type" : "string",
          "description" : "Bearer asdf2364saf8796324xcihkj23h42oi34"
        }, {
          "name" : "body",
          "in" : "body",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/Question"
          },
          "x-examples" : {
            "application/json" : "{text: \"The updated text to be provided for this question.\"}"
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "Updated successfully."
          },
          "401" : {
            "description" : "Missing or invalid authorization token."
          },
          "403" : {
            "description" : "If the owner of the question tries to vote the question."
          },
          "404" : {
            "description" : "The entity is not found."
          },
          "422" : {
            "description" : "If the JSON body does not contain the required attributes."
          }
        }
      },
      "parameters" : [ {
        "name" : "id",
        "in" : "path",
        "required" : true,
        "type" : "string"
      } ],
      "x-restlet" : {
        "section" : "Question APIs"
      }
    },
    "/questions/{id}/comments" : {
      "post" : {
      	"tags": [
          "question-comment"
        ],
        "summary" : "Post comment for a question",
        "description" : "Post a comment for a question.",
        "consumes" : [ "application/json" ],
        "parameters" : [ {
          "name" : "Content-Type",
          "in" : "header",
          "required" : false,
          "type" : "string",
          "description" : "application/json"
        }, {
          "name" : "Authorization",
          "in" : "header",
          "required" : true,
          "type" : "string",
          "description" : "Bearer asdf2364saf8796324xcihkj23h42oi34"
        }, {
          "name" : "body",
          "in" : "body",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/Comment"
          }
        } ],
        "responses" : {
          "201" : {
            "description" : "Comment posted successfully.",
            "headers" : {
              "Location" : {
                "type" : "string",
                "description" : "/comments/{id}",
                "x-example" : "/comments/4455-2212-3345-7765"
              }
            }
          },
          "401" : {
            "description" : "If user is not authenticated, due to missing or incorrect Authorization header."
          },
          "422" : {
            "description" : "The POST body did not contain all the required attributes."
          }
        }
      },
      "parameters" : [ {
        "name" : "id",
        "in" : "path",
        "required" : true,
        "type" : "string"
      } ],
      "x-restlet" : {
        "section" : "Question APIs"
      }
    },
    "/questions/{id}/comments/{id}" : {
      "patch" : {
      	"tags": [
          "question-comment"
        ],
        "summary" : "Update comment for question",
        "description" : "Update a comment for a question:\n1. Update the content\n2. Vote up\n3. Vote down",
        "consumes" : [ "application/json" ],
        "parameters" : [ {
          "name" : "action",
          "in" : "query",
          "required" : false,
          "type" : "string",
          "description" : "1. For vote up: ?action=VOTE_UP\n2. For vote down: ?action=VOTE_DOWN\n3. For content update: ?action=CONTENT_UPDATE"
        }, {
          "name" : "Content-Type",
          "in" : "header",
          "required" : false,
          "type" : "string",
          "description" : "application/json"
        }, {
          "name" : "Authorization",
          "in" : "header",
          "required" : true,
          "type" : "string",
          "description" : "Bearer asdf2364saf8796324xcihkj23h42oi34"
        }, {
          "name" : "body",
          "in" : "body",
          "required" : true,
          "schema" : {
            "type" : "object"
          },
          "x-examples" : {
            "application/json" : "Owner of comment updates the content: {text: \"the updated answer.\"}"
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "Status 200"
          },
          "401" : {
            "description" : "Missing or invalid authorization token."
          },
          "403" : {
            "description" : "If an user other than the owner of the comment tries to update the content."
          },
          "404" : {
            "description" : "Entity not found."
          },
          "422" : {
            "description" : "If the JSON body does not contain the required attributes."
          }
        }
      },
      "parameters" : [ {
        "name" : "id",
        "in" : "path",
        "required" : true,
        "type" : "string"
      } ],
      "x-restlet" : {
        "section" : "Question APIs"
      }
    },
    "/questions/{id}/answers" : {
      "post" : {
      	"tags": [
          "answer"
        ],
        "summary" : "Post an answer",
        "description" : "Post an answer for the given question.",
        "consumes" : [ "application/json" ],
        "parameters" : [ {
          "name" : "Content-Type",
          "in" : "header",
          "required" : false,
          "type" : "string",
          "description" : "application/json"
        }, {
          "name" : "Authorization",
          "in" : "header",
          "required" : true,
          "type" : "string",
          "description" : "Bearer asdf2364saf8796324xcihkj23h42oi34"
        }, {
          "name" : "body",
          "in" : "body",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/Answer"
          }
        } ],
        "responses" : {
          "201" : {
            "description" : "Answer posted successfully.",
            "headers" : {
              "Location" : {
                "type" : "string",
                "description" : "ID of the answer object.",
                "x-example" : "4455-2212-3345-7765"
              }
            }
          },
          "401" : {
            "description" : "Missing or invalid authorization token."
          },
          "422" : {
            "description" : "The POST body did not contain all the required attributes."
          }
        }
      },
      "parameters" : [ {
        "name" : "id",
        "in" : "path",
        "required" : true,
        "type" : "string"
      } ],
      "x-restlet" : {
        "section" : "Answer APIs"
      }
    },
    "/questions/answers/{id}" : {
      "patch" : {
      	"tags": [
          "answer"
        ],
        "summary" : "Update answer",
        "description" : "Scenarios:\n1. Update content\n2. Vote up\n3. Vote down\n4. Owner of *question* wants to set this answer as the best one.",
        "consumes" : [ "application/json" ],
        "parameters" : [ {
          "name" : "action",
          "in" : "query",
          "required" : false,
          "type" : "string",
          "description" : "1. For vote up: ?action=VOTE_UP\n2. For vote down: ?action=VOTE_DOWN\n3. For content update: ?action=CONTENT_UPDATE\n4. To set best answer: ?action=SET_ANSWER"
        }, {
          "name" : "Content-Type",
          "in" : "header",
          "required" : false,
          "type" : "string",
          "description" : "application/json"
        }, {
          "name" : "Authorization",
          "in" : "header",
          "required" : true,
          "type" : "string",
          "description" : "Bearer asdf2364saf8796324xcihkj23h42oi34"
        }, {
          "name" : "body",
          "in" : "body",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/Answer"
          },
          "x-examples" : {
            "application/json" : "Owner of answer updates the content: {text: \"the updated answer.\"}"
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "Status 200"
          },
          "401" : {
            "description" : "Missing or invalid authorization token."
          },
          "403" : {
            "description" : "1. If an user other than the owner of the question tries to mark this as the best answer.\n2. If an user other than the owner of the answer tries to update the content."
          },
          "404" : {
            "description" : "Entity not found."
          },
          "422" : {
            "description" : "If the JSON body does not contain the required attributes."
          }
        }
      },
      "parameters" : [ {
        "name" : "id",
        "in" : "path",
        "required" : true,
        "type" : "string"
      } ],
      "x-restlet" : {
        "section" : "Answer APIs"
      }
    },
    "/questions/answers/{id}/comments" : {
      "post" : {
      	"tags": [
          "answer-comment"
        ],
        "summary" : "Post comment for an answer",
        "description" : "Post a comment for an answer.",
        "consumes" : [ "application/json" ],
        "parameters" : [ {
          "name" : "Content-Type",
          "in" : "header",
          "required" : false,
          "type" : "string",
          "description" : "application/json"
        }, {
          "name" : "Authorization",
          "in" : "header",
          "required" : true,
          "type" : "string",
          "description" : "Bearer asdf2364saf8796324xcihkj23h42oi34"
        }, {
          "name" : "body",
          "in" : "body",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/Comment"
          }
        } ],
        "responses" : {
          "201" : {
            "description" : "Comment posted successfully.",
            "headers" : {
              "Location" : {
                "type" : "string",
                "description" : "/comments/{id}",
                "x-example" : "/comments/4455-2212-3345-7765"
              }
            }
          },
          "401" : {
            "description" : "If user is not authenticated, due to missing or incorrect Authorization header."
          },
          "422" : {
            "description" : "The POST body did not contain all the required attributes."
          }
        }
      },
      "parameters" : [ {
        "name" : "id",
        "in" : "path",
        "required" : true,
        "type" : "string"
      } ],
      "x-restlet" : {
        "section" : "Answer APIs"
      }
    },
    "/questions/answers/comments/{id}" : {
      "patch" : {
      	"tags": [
          "answer-comment"
        ],
        "summary" : "Update comment for an answer",
        "description" : "Update a comment for an answer:\n1. Update the content\n2. Vote up\n3. Vote down",
        "consumes" : [ "application/json" ],
        "parameters" : [ {
          "name" : "action",
          "in" : "query",
          "required" : false,
          "type" : "string",
          "description" : "1. For vote up: ?action=VOTE_UP\n2. For vote down: ?action=VOTE_DOWN\n3. For content update: ?action=CONTENT_UPDATE"
        }, {
          "name" : "Content-Type",
          "in" : "header",
          "required" : false,
          "type" : "string",
          "description" : "application/json"
        }, {
          "name" : "Authorization",
          "in" : "header",
          "required" : true,
          "type" : "string",
          "description" : "Bearer asdf2364saf8796324xcihkj23h42oi34"
        }, {
          "name" : "body",
          "in" : "body",
          "required" : true,
          "schema" : {
            "type" : "object"
          },
          "x-examples" : {
            "application/json" : "Owner of comment updates the content: {text: \"the updated answer.\"}"
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "Status 200"
          },
          "401" : {
            "description" : "Missing or invalid authorization token."
          },
          "403" : {
            "description" : "If an user other than the owner of the comment tries to update the content."
          },
          "404" : {
            "description" : "Entity not found."
          },
          "422" : {
            "description" : "If the JSON body does not contain the required attributes."
          }
        }
      },
      "parameters" : [ {
        "name" : "id",
        "in" : "path",
        "required" : true,
        "type" : "string"
      } ],
      "x-restlet" : {
        "section" : "Answer APIs"
      }
    }
  },
  "definitions" : {
    "User" : {
      "type" : "object",
      "required" : [ "firstName", "lastName" ],
      "properties" : {
        "_id" : {
          "type" : "string",
          "description" : "Email address used as identifier as well as username for login."
        },
        "firstName" : {
          "type" : "string",
          "description" : "First name"
        },
        "lastName" : {
          "type" : "string",
          "description" : "Last name"
        },
        "createdTime" : {
          "type" : "number",
          "description" : "Created time"
        },
        "password" : {
          "type" : "string",
          "description" : "Password for the user."
        },
        "lastUpdatedTime" : {
          "type" : "string",
          "description" : "Last updated time"
        }
      },
      "description" : "*Sample JSON Structure:*\n```\n\t{\n\t\t_id: \"john.doe@stackoverflow.com\", // Email address will be the ID.\n\t\tfirstName: \"John\",\n\t\tlastName: \"Doe\",\n\t\tpassword: \"<one-way-hashed-value>\",\n\t\tcreatedTime: 1493720595286,\n\t\tlastUpdatedTime: 1493720595286\n\t}\n```",
      "x-restlet" : {
        "section" : "Data model"
      }
    },
    "Question" : {
      "type" : "object",
      "required" : [ "text" ],
      "properties" : {
        "_id" : {
          "type" : "string",
          "description" : "Identifier"
        },
        "userId" : {
          "type" : "string",
          "description" : "The identifier of the user who posted this question."
        },
        "userName" : {
          "type" : "string",
          "description" : "The display name of the user who posted this question."
        },
        "lastUpdatedUserId" : {
          "type" : "string",
          "description" : "Identifier of the user who last updated this question."
        },
        "lastUpdatedUserName" : {
          "type" : "string",
          "description" : "Display name of the user who last updated this question."
        },
        "text" : {
          "type" : "string",
          "description" : "The answer content."
        },
        "tags" : {
          "type" : "array",
          "description" : "Any tags relevant to the answer",
          "items" : {
            "type" : "string"
          }
        },
        "voteCount" : {
          "type" : "integer",
          "description" : "Count of votes for this question."
        },
        "createdTime" : {
          "type" : "number"
        },
        "lastUpdatedTime" : {
          "type" : "number"
        },
        "viewCount" : {
          "type" : "integer",
          "description" : "View count for this question. Only gets incremented if a questions is retrieved by ID."
        },
        "comments" : {
          "type" : "array",
          "description" : "List of comments for this question.",
          "items" : {
            "$ref" : "#/definitions/Comment"
          }
        },
        "answers" : {
          "type" : "array",
          "description" : "List of answers for this question.",
          "items" : {
            "$ref" : "#/definitions/Answer"
          }
        }
      },
      "description" : "*Sample JSON Structure:*\n1. When posted:\n```\n\t{\n\t\ttext: \"This is the content of a sample answer\",\n\t\ttags: [\"sample\"],\n\t}\n```\n2. When retrieved:\n```\n\t{\n\t\t_id: \"1234-5678-9012-3456\",\n\t\tuserId: \"billswift@stackoverflow.com\",\n\t\tuserName: \"Bill Swift\",\n\t\tlastUpdatedUserId: \"johndoe@stackoverflow.com\",\n\t\tlastUpdatedUserName: \"John Doe\",\n\t\ttext: \"This is the content of a sample question\",\n\t\ttags: [\"sample\"],\n\t\tvoteCount: 654,\n\t\tcreatedTime: 1493719334210,\n\t\tlastUpdatedTime: 1493719362860,\n\t\tviewCount: 4532,\n\t\tcomments: [{\n\t\t\t_id: \"1234-5678-9012-3456\",\n\t\t\tuserId: \"jenniferstorm@stackoverflow.com\",\n\t\t\tuserDisplayName: \"Jennifer Storm\",\n\t\t\ttext: \"This is the content of a sample comment\",\n\t\t\ttags: [\"sample\"],\n\t\t\tvoteCount: 23,\n\t\t\tcreatedTime: 1493719334210,\n\t\t\tlastUpdatedTime: 1493719362860\n\t\t}],\n\t\tanswers: [{\n\t\t\t_id: \"1234-5678-9012-3456\",\n\t\t\tuserId: \"anneblack@stackoverflow.com\",\n\t\t\tuserDisplayName: \"Anne Black\",\n\t\t\ttext: \"This is the content of a sample answer\",\n\t\t\ttags: [\"sample\"],\n\t\t\tvoteCount: 45,\n\t\t\tcreatedTime: 1493719334210,\n\t\t\tlastUpdatedTime: 1493719362860,\n\t\t\tisSelectedAnswer: false,\n\t\t\tcomments: [{\n\t\t\t\t_id: \"1234-5678-9012-3456\",\n\t\t\t\tuserId: \"johndoe@stackoverflow.com\",\n\t\t\t\tuserDisplayName: \"John Doe\",\n\t\t\t\ttext: \"This is the content of a sample comment\",\n\t\t\t\ttags: [\"sample\"],\n\t\t\t\tvoteCount: 23,\n\t\t\t\tcreatedTime: 1493719334210,\n\t\t\t\tlastUpdatedTime: 1493719362860\n\t\t\t}]\n\t\t}]\n\t}\n```",
      "x-restlet" : {
        "section" : "Data model"
      }
    },
    "Answer" : {
      "type" : "object",
      "required" : [ "text" ],
      "properties" : {
        "_id" : {
          "type" : "string",
          "description" : "Identifier"
        },
        "userId" : {
          "type" : "string",
          "description" : "The identifier of the user who entered this answer."
        },
        "userName" : {
          "type" : "string",
          "description" : "The display name of the user."
        },
        "text" : {
          "type" : "string",
          "description" : "The answer content."
        },
        "tags" : {
          "type" : "array",
          "description" : "Any tags relevant to the answer",
          "items" : {
            "type" : "string"
          }
        },
        "voteCount" : {
          "type" : "integer"
        },
        "createdTime" : {
          "type" : "number"
        },
        "lastUpdatedTime" : {
          "type" : "number"
        },
        "isSelectedAnswer" : {
          "type" : "boolean",
          "description" : "Is this the selected answer?"
        },
        "comments" : {
          "type" : "array",
          "description" : "List of comments for this answer.",
          "items" : {
            "$ref" : "#/definitions/Comment"
          }
        }
      },
      "description" : "*Sample JSON Structure:*\n\n1. When posted:\n```\n\t{\n\t\ttext: \"This is the content of a sample answer\",\n\t\ttags: [\"sample\"]\n\t}\n```\n2. When retrieved:\n```\n\t{\n\t\t_id: \"1234-5678-9012-3456\",\n\t\tuserId: \"smithblack@stackoverflow.com\",\n\t\tuserName: \"Smith Black\",\n\t\ttext: \"This is the content of a sample answer\",\n\t\ttags: [\"sample\"],\n\t\tvoteCount: 45,\n\t\tcreatedTime: 1493719334210,\n\t\tlastUpdatedTime: 1493719362860,\n\t\tisSelectedAnswer: false,\n\t\tcomments: [{\n\t\t\t_id: \"1234-5678-9012-3456\",\n\t\t\tuserId: \"6754-0987-6754-3289\",\n\t\t\tuserDisplayName: \"John Doe\",\n\t\t\ttext: \"This is the content of a sample comment\",\n\t\t\ttags: [\"sample\"],\n\t\t\tvoteCount: 23,\n\t\t\tcreatedTime: 1493719334210,\n\t\t\tlastUpdatedTime: 1493719362860\n\t\t}]\n\t}\n```",
      "x-restlet" : {
        "section" : "Data model"
      }
    },
    "Comment" : {
      "type" : "object",
      "required" : [ "text" ],
      "properties" : {
        "_id" : {
          "type" : "string",
          "description" : "Identifier"
        },
        "userId" : {
          "type" : "string",
          "description" : "The identifier of the user who entered this comment."
        },
        "userName" : {
          "type" : "string",
          "description" : "The display name of the user."
        },
        "text" : {
          "type" : "string",
          "description" : "The comment content."
        },
        "tags" : {
          "type" : "array",
          "description" : "Any tags relevant to the comment",
          "items" : {
            "type" : "string"
          }
        },
        "voteCount" : {
          "type" : "integer"
        },
        "createdTime" : {
          "type" : "number"
        },
        "lastUpdatedTime" : {
          "type" : "number"
        }
      },
      "description" : "*Sample JSON Structure:*\n1. When posted:\n```\n\t{\n\t\ttext: \"This is the content of a sample comment\",\n\t\ttags: [\"sample\"]\n\t}\n```\n\n2. When retrieved:\n```\n\t{\n\t\t_id: \"1234-5678-9012-3456\",\n\t\tuserId: \"johndoe@stackoverflow.com\",\n\t\tuserName: \"John Doe\",\n\t\ttext: \"This is the content of a sample comment\",\n\t\ttags: [\"sample\"],\n\t\tvoteCount: 23,\n\t\tcreatedTime: 1493719334210,\n\t\tlastUpdatedTime: 1493719362860\n\t}\n```",
      "x-restlet" : {
        "section" : "Data model"
      }
    }
  },
  "x-restlet" : {
    "sections" : {
      "General" : {
        "description" : "The API attempts to adhere to standard REST endpoint best practices. Here are a few explicit statements to avoid ambiguity\n\n* GET without any identifier returns all instances. The result will be paginated.\n* HTTP PATCH method will be used when changing only a portion of an entity. PUT will be used only when replacing the full entity.\n* URLs will use the plural form of the entity name. Example, /questions will be used for all question related endpoints.\n* Unless explicitly stated, all content in request & response body will be in JSON format."
      },
      "Data model" : { },
      "User APIs" : { },
      "Question APIs" : { },
      "Answer APIs" : { },
      "Roadmap" : { }
    },
    "texts" : {
      "p67mo6q" : {
        "title" : "Authentication",
        "content" : "The API is secured using a custom JWT token based authentication. \nAll **read operations are open** and don't require authentication. \nHowever, all **write operations require authentication**. The user needs to login to receive a JWT token. The JWT token is expected as an 'Authorization' header in all write operations (POST, PUT, PATCH, DELETE).\n<br/>Example: <code>Authorization : Bearer 324qwe67sdfa3246jksadf80979s</code>",
        "section" : "General"
      },
      "oerttk6" : {
        "title" : "HTTP Request",
        "content" : "\n| Section | Description |\n| -------- | -------- | \n| Methods     | Supports : GET, POST, PUT, PATCH, DELETE. POST is used to create and PUT is used to replace. PATCH is used to change a portion of an entity.   | \n| URL     | Uses the plural form of the entity. Example : /questions is used for question entity. <br>The identifier of the entity is expected as part of the URI path. Example: GET /questions/{id} | \n| Headers     |  content-type: application / json . <br>Accept: application / json. <br>Authorization : Bearer <JWT_TOKEN> is required for all write operations (POST, PUT, PATCH, DELETE).   | \n| Request Parameters     | Search and pagination attributes are expected as request parameters. Example: GET /questions?text=vertx&page=1&size=10    |\n| Body     | Request body is expected to be in JSON format. Data sent to server via POST, PUT, PATCH are expected to be in the request body.   | \n\n\n\n",
        "section" : "General"
      },
      "earjdx1" : {
        "title" : "HTTP Response",
        "content" : "The API follows standard HTTP response codes to send back status information. Data is returned in JSON format, unless specifically mentioned. Headers may be used to return identifiers in case of POST operation.\n\n| Status Code | Description |\n| -------- | -------- | \n|  200 | Success for GET, PUT, PATCH, DELETE |\n|  201 | Sucessful POST operation | \n|  304 | Not modified. Mainly used for static content, if any. | \n|  400 | Bad request. Syntax error in the JSON format | \n|  401| Unauthorized. Returned if missing of invalid JWT authorization token for write operations. |\n|  403| Forbidden: Authenticated user, but unauthorized to perform the given operation - based on roles. |\n|  404| Not found, may be because of invalid identifiers. Note, search operations with no results still returns 200. |\n|  422| Unprocessable entity. The request body has a valid JSON object, but the attributes are not valid for the given resource. |\n|  500| Internal server error, caused by code or system malfunction |\n|  503| One of the downstream servers (App, DB, etc) is unavailable. |",
        "section" : "General"
      },
      "6cyn67a" : {
        "title" : "Further APIs",
        "content" : "We thought about a few additional APIs that may be implemented in the future.\n1. Role definitions for users.\n2. A statistics entity to keep track of activities like user sessions, questions, answers, comments, votes, search hits, etc.\n\nWe are also thinking of exporting this API into a swagger or a RAML file and use it as an input to do 'Contract-First' implementation of the APIs.\n\n\n\n\n\n\n\n\n\n",
        "section" : "Roadmap"
      }
    }
  }
}
