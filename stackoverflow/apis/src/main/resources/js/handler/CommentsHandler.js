/**
 * Handles operations related to comments.
 */
var logger = Java.type("io.vertx.core.logging.LoggerFactory").getLogger("js.handler.CommentsHandler");
var ANSWERS = "answer";
var QUESTIONS = 'question';
var CommentsHandler = {
		postComment : function(rc, type) {
			console.log("Posting comment for " + type);
	    	
	    	var input = rc.getBodyAsJson();
	    	
	    	var comment = {};
	    	
	    	// Validate input attributes and populate domain model.
	        if (input == null) {
	            rc.response().setStatusCode(422).setStatusMessage("Empty request").end();
	            return;
	        }
	        
	        if (input.text && typeof input.text === 'string' && input.text.trim().length > 0) {
	        	comment.text = input.text;
	        } else {
	            rc.response().setStatusCode(422).setStatusMessage("Attribute 'text' is empty.").end();
	            return;
	        }
	        
	        comment.userId = "derpaul@cisco.com";
	        comment.userName = "Derrick Paul";

	        comment.createdTime = (new Date()).getTime();
	        comment.entityId = rc.request().getParam(type + "Id");
	        
	        var channel = "com.cisco.cmad.so." + type + "s.comments.post";
	        
	        vertx.eventBus().send(channel, comment, function(result, error) {
	            if (error) {
	                rc.fail(error);
	            } else {
	                rc.response().setStatusCode(201).putHeader("location", result.body()).end();
	            }
	        });
		},
		postCommentOnQuestion : function(rc) {
			CommentsHandler.postComment(rc, QUESTIONS);
    },
    updateComment : function(rc, type) {
		console.log("Updating comment for " + type);
    	
    	var input = rc.getBodyAsJson();
    	
    	var comment = {};
    	
    	// Validate input attributes and populate domain model.
        if (input == null) {
            rc.response().setStatusCode(422).setStatusMessage("Empty request").end();
            return;
        }
        
        if (input.text && typeof input.text === 'string' && input.text.trim().length > 0) {
        	comment.text = input.text;
        } else {
            rc.response().setStatusCode(422).setStatusMessage("Attribute 'text' is empty.").end();
            return;
        }
        
        comment.lastUpdatedUserId = "derpaul@cisco.com";
        comment.lastUpdatedUserName = "Derrick Paul";

        comment.lastUpdatedTime = (new Date()).getTime();
        comment.commentId = rc.request().getParam("commentId");
        
        var channel = "com.cisco.cmad.so." + type + "s.comments.update";
        
        vertx.eventBus().send(channel, comment, function(result, error) {
            if (error) {
                rc.fail(error);
            } else {
                rc.response().setStatusCode(200).end();
            }
        });
	},
	postCommentOnQuestion : function(rc) {
		CommentsHandler.postComment(rc, QUESTIONS);
},
    updateCommentOnQuestion : function(rc) {
    	CommentsHandler.updateComment(rc, QUESTIONS);
    },
    postCommentOnAnswer : function(rc) {
    	CommentsHandler.postComment(rc, ANSWERS);
    },
    updateCommentOnAnswer : function(rc) {
    	CommentsHandler.updateComment(rc, ANSWERS);
    }
}