/**
 * Handles operations related to user and authentication.
 */
var logger = Java.type("io.vertx.core.logging.LoggerFactory").getLogger("js.handler.UsersHandler");

var UsersHandler = {
    createUser : function(rc) {		
        var input = rc.getBodyAsJson();
		logger.info("User input:");
        var user = {};

        // Validate input attributes and populate domain model.
        if (input == null) {
            rc.response().setStatusCode(422).setStatusMessage("Empty rcuest").end();
            return;
        }

        if (input.userName && typeof input.userName === 'string' && input.userName.trim().length > 0) {
            user.userName = input.userName;			
        } else {
            rc.response().setStatusCode(422).setStatusMessage("Mandatory attribute 'userName' is empty.").end();
            return;
        }

        if (input.firstName && typeof input.firstName === 'string' && input.firstName.trim().length > 0) {
            user.firstName = input.firstName;
        } else {
            rc.response().setStatusCode(422).setStatusMessage("Mandatory attribute 'firstName' is empty.").end();
            return;
        }

        if (input.password && typeof input.password === 'string' && input.password.trim().length > 0) {
            user.password = input.password;
        } else {
            rc.response().setStatusCode(422).setStatusMessage("Mandatory attribute 'password' is empty.").end();
            return;
        }
		
		if (input.lastName && typeof input.lastName === 'string' && input.lastName.trim().length > 0) {
            user.lastName = input.lastName;
        } else {
            user.lastName = "";            
        }
		
		user.createdTime = (new Date()).getTime();
		user.lastUpdatedTime = (new Date()).getTime();

		
        vertx.eventBus().send("com.cisco.cmad.so.users.post", user , function(result, error) {
            if (error) {
                rc.fail(error);
            } else {
                rc.response().setStatusCode(201).putHeader("location", result.body()).end();
            }
        });
		
    },
    searchUsers : function(rc) {

    },
    getUserById : function(rc) {

    },
    updateUser : function(rc) {

    },
    generateToken : function(rc) {
		var input = rc.getBodyAsJson();		
		var userName;
		var password;
		var user = {};
		
	    if (input.userName && typeof input.userName === 'string' && input.userName.trim().length > 0) {
            user.userName = input.userName;
			
        } else {
            rc.response().setStatusCode(422).setStatusMessage("Mandatory attribute 'userName' is empty.").end();
            return;
        }

		if (input.password && typeof input.password === 'string' && input.password.trim().length > 0) {
           user.password = input.password;			
        } else {
            rc.response().setStatusCode(422).setStatusMessage("Mandatory attribute 'password' is empty.").end();
            return;
        }		
		
		logger.info("Sending message to authenticate user"+user);
		
		vertx.eventBus().send("com.cisco.cmad.so.users.authenticate", user , function(result, error) {
            if (error) {
                rc.fail(error);
			} else {
				var respJson = JSON.parse(result.body());				 
				var jwtToken = respJson.jwtToken;
				
				if(jwtToken)
				    rc.response().setStatusCode(201).putHeader("Authorization", jwtToken).end();
				else
					rc.response().setStatusCode(401).setStatusMessage("Invalid User/password").end();
            }
        });
    }
}