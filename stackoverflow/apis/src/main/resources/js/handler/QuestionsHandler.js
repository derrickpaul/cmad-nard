/**
 * Handles operations related to questions.
 */
var logger = Java.type("io.vertx.core.logging.LoggerFactory").getLogger("js.handler.QuestionsHandler");
var QuestionsHandler = {
    postQuestion : function(rc) {
        var input = rc.getBodyAsJson();
        var question = {};

        // Validate input attributes and populate domain model.
        if (input == null) {
            rc.response().setStatusCode(422).setStatusMessage("Empty request").end();
            return;
        }

        if (input.text && typeof input.text === 'string' && input.text.trim().length > 0) {
            question.text = input.text;
        } else {
            rc.response().setStatusCode(422).setStatusMessage("Attribute 'text' is empty.").end();
            return;
        }

        if (input.tags) {
            if (Array.isArray(input.tags)) {
                question.tags = input.tags.toString().split(",");
            } else {
                rc.response().setStatusCode(422).setStatusMessage(
                        "Attribute 'tags' if present, has to be an array of String").end();
                return;
            }
        }

        // Populate user and meta-data attributes.

        // TODO : Take user info from principal.
        // var principal = rc.user().principal();
        // question.userId = principal.userId;
        // question.userName = principal.userName;

        question.userId = "derpaul@cisco.com";
        question.userName = "Derrick Paul";

        question.createdTime = (new Date()).getTime();
        question.lastUpdatedTime = question.createdTime;
        question.voteCount = 0;
        question.viewCount = 0;
        question.comments = [];
        question.answers = [];

        vertx.eventBus().send("com.cisco.cmad.so.questions.post", question, function(result, error) {
            if (error) {
                rc.fail(error);
            } else {
                rc.response().setStatusCode(201).putHeader("location", result.body()).end();
            }
        });
    },
    searchQuestions : function(rc) {
        var text = rc.request().getParam("text");
        var skip = rc.request().getParam("skip");
        var limit = rc.request().getParam("limit");

        try {
            if (skip == null || skip.trim().length == 0) {
                skip = 0;
            } else {
                skip = parseInt(skip);
            }
            if (skip < 0)
                throw "Invalid skip value.";

            if (limit == null || limit.trim().length == 0) {
                limit = 10;
            } else {
                limit = parseInt(limit);
            }
            if (limit < 1 || limit > 100)
                throw "Invalid limit value.";

        } catch (e) {
            rc.response().setStatusCode(400).setStatusMessage(
                    "Bad request: skip has to be a positive integer and limit has to be between 1 and 100").end();
            return;
        }

        var searchJson = {
            text : text,
            skip : skip,
            limit : limit
        };

        vertx.eventBus().send("com.cisco.cmad.so.questions.search", searchJson, function(result, error) {
            if (error) {
                rc.fail(error);
            } else {
                rc.response().setStatusCode(200).write(result.body()).end();
            }
        });
    },
    getQuestionById : function(rc) {
        var questionId = rc.request().getParam("questionId");
        if (questionId == null || questionId.trim().length == 0) {
            rc.response().setStatusCode(404).setStatusMessage("Empty question ID").end();
            return;
        }

        vertx.eventBus().send("com.cisco.cmad.so.questions.get", questionId, function(result, error) {
            if (error) {
                rc.fail(error);
            } else if (result.body() == null) {
                rc.response().setStatusCode(404).setStatusMessage("Not found").end();
            } else {
                rc.response().setStatusCode(200).write(result.body()).end();
            }
        });
    },
    updateQuestion : function(rc) {
    	logger.debug("Patching question...");
    	
        var question = {};
        
        question.lastUpdatedUserId = "derpaul@cisco.com";
        question.lastUpdatedUserName = "Derrick Paul";
        question.lastUpdatedTime = (new Date()).getTime();
        question.questionId = rc.request().getParam("questionId");
        
        var action = rc.request().getParam("action");
        var event = "com.cisco.cmad.so.questions.update";
        
        if (!action || action == 'CONTENT_UPDATE') {
        	var input = rc.getBodyAsJson();

        	if (input == null) {
        		rc.response().setStatusCode(422).setStatusMessage("Empty request").end();
        		return;
        	}
        	
	        if (input.text && typeof input.text === 'string' && input.text.trim().length > 0) {
	        	question.text = input.text;
	        } else {
	            rc.response().setStatusCode(422).setStatusMessage("Attribute 'text' is empty.").end();
	            return;
	        }
        } else if (action && action == 'VOTE_UP') {
        	event = "com.cisco.cmad.so.questions.vote";
        	question.vote = 1;
        } else if (action && action == 'VOTE_DOWN') {
        	event = "com.cisco.cmad.so.questions.vote";
        	question.vote = -1;
        } else if (action && action == "SET_ANSWER") {
        	// TODO the correct answer should be set here.
        }

        logger.debug("Send event to java handler...")
        vertx.eventBus().send(event, question, function(result, error) {
            if (error) {
                rc.fail(error);
            } else {
                rc.response().setStatusCode(200).end();
            }
        });
    }
}