/**
 * Handles operations related to answers.
 */
var logger = Java.type("io.vertx.core.logging.LoggerFactory").getLogger("js.handler.AnswersHandler");
var AnswersHandler = {

    postAnswer : function(rc) {
    	logger.debug("Posting answer...");
    	var input = rc.getBodyAsJson();
		// publish an event on 'com.cisco.cmad.so.answers.post' with answer json. 
		
        var answer = {};

        // Validate input attributes and populate domain model.
        if (input == null) {
            rc.response().setStatusCode(422).setStatusMessage("Empty request").end();
            return;
        }

        if (input.text && typeof input.text === 'string' && input.text.trim().length > 0) {
        	answer.text = input.text;
        } else {
            rc.response().setStatusCode(422).setStatusMessage("Attribute 'text' is empty.").end();
            return;
        }

        // Populate user and meta-data attributes.

        // TODO : Take user info from principal.
        // var principal = rc.user().principal();
        // question.userId = principal.userId;
        // question.userName = principal.userName;

        answer.userId = "derpaul@cisco.com";
        answer.userName = "Derrick Paul";

        answer.createdTime = (new Date()).getTime();
        answer.voteCount = 0;
        answer.questionId = rc.request().getParam("questionId"); 

        logger.debug("Send event to java handler...")
        vertx.eventBus().send("com.cisco.cmad.so.answers.post", answer, function(result, error) {
            if (error) {
                rc.fail(error);
            } else {
                rc.response().setStatusCode(201).putHeader("location", result.body()).end();
            }
        });
    },
    updateAnswer : function(rc) {
    	logger.debug("Patching answer...");
    	
        var answer = {};

        answer.lastUpdatedUserId = "derpaul@cisco.com";
        answer.lastUpdatedUserName = "Derrick Paul";
        answer.lastUpdatedTime = (new Date()).getTime();
        answer.answerId = rc.request().getParam("answerId");
        
        var action = rc.request().getParam("action");
        var event = "com.cisco.cmad.so.answers.update";
        
        if (!action || action == 'CONTENT_UPDATE') {
        	var input = rc.getBodyAsJson();

        	if (input == null) {
        		rc.response().setStatusCode(422).setStatusMessage("Empty request").end();
        		return;
        	}
        	
	        if (input.text && typeof input.text === 'string' && input.text.trim().length > 0) {
	        	answer.text = input.text;
	        } else {
	            rc.response().setStatusCode(422).setStatusMessage("Attribute 'text' is empty.").end();
	            return;
	        }
        } else if (action && action == 'VOTE_UP') {
        	event = "com.cisco.cmad.so.answers.vote";
        	answer.vote = 1;
        } else if (action && action == 'VOTE_DOWN') {
        	event = "com.cisco.cmad.so.answers.vote";
        	answer.vote = -1;
        } else if (action && action == "SET_ANSWER") {
        	
        }

        logger.debug("Send event to java handler...")
        vertx.eventBus().send(event, answer, function(result, error) {
            if (error) {
                rc.fail(error);
            } else {
                rc.response().setStatusCode(200).end();
            }
        });
    }
    
    
}