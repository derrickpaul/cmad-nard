package com.cisco.cmad.so.service;

import com.cisco.cmad.so.util.MongoUtil;

import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.mongo.FindOptions;

public class QuestionsService {

    private static Logger logger = LoggerFactory.getLogger(QuestionsService.class.getName());

    public static final String QUESTIONS = "questions";
	private static final String QUESTION_ID = "questionId";

    public void postQuestion(Message<Object> message) {
        logger.debug("Posting question to database.");
        JsonObject document = new JsonObject(message.body().toString());

        MongoUtil.getMongoClient().insert(QUESTIONS, document, reply -> {
            if (reply.succeeded()) {
                message.reply(reply.result());
            } else {
                logger.error("Error posting question to database", reply.cause());
                message.fail(500, "Database error.");
            }
        });
    }

    public void searchQuestions(Message<Object> message) {
        logger.debug("Search questions from database.");
        JsonObject document = new JsonObject(message.body().toString());

        int skip = document.getInteger("skip");
        int limit = document.getInteger("limit");
        String text = document.getString("text");

        FindOptions findOptions = new FindOptions();
        // Apply skip and limit.
        findOptions.setLimit(limit);
        findOptions.setSkip(skip);

        JsonObject query = new JsonObject();
        JsonObject sort = new JsonObject();
        JsonObject fields = new JsonObject();

        // Do not retrieve answers and comments.
        fields.put("answers", 0);
        fields.put("comments", 0);

        // Search by question text.
        // If no text is given, get all questions
        if (text != null && text.trim().length() > 0) {
            JsonObject subQuery = new JsonObject();
            subQuery.put("$search", text);
            query.put("$text", subQuery);

            // Include search score metadata in the result
            JsonObject metaField = new JsonObject();
            metaField.put("$meta", "textScore");
            fields.put("searchScore", metaField);

            // Sort by relevance and then by date.
            sort.put("searchScore", metaField);
        }

        sort.put("voteCount", -1);
        sort.put("viewCount", -1);
        findOptions.setFields(fields);
        findOptions.setSort(sort);

        JsonObject result = new JsonObject();

        MongoUtil.getMongoClient().count(QUESTIONS, query, countReply -> {
            if (countReply.succeeded()) {
                result.put("total", countReply.result());
                MongoUtil.getMongoClient().findWithOptions(QUESTIONS, query, findOptions, searchReply -> {
                    if (searchReply.succeeded()) {
                        JsonArray resultArray = new JsonArray(searchReply.result());
                        result.put("data", resultArray);
                        message.reply(result.toString());
                    } else {
                        logger.error("Error searching questions from database", searchReply.cause());
                        message.fail(500, "Database error.");
                    }
                });
            } else {
                logger.error("Error counting search questions from database", countReply.cause());
                message.fail(500, "Database error.");
            }
        });
    }

    public void getQuestionById(Message<Object> message) {
        logger.debug("Get question by Id");
        String questionId = message.body().toString();
        JsonObject query = new JsonObject();
        query.put("_id", questionId);

        // Increment view count by 1 for each by question by Id.
        JsonObject update = new JsonObject();
        JsonObject updateViewCount = new JsonObject();
        updateViewCount.put("viewCount", 1);
        update.put("$inc", updateViewCount);

        MongoUtil.getMongoClient().findOneAndUpdate(QUESTIONS, query, update, reply -> {
            if (reply.succeeded()) {
                if (reply.result() == null)
                    message.reply(null);
                else
                    message.reply(reply.result().toString());
            } else {
                logger.error("Error getting question by ID from database", reply.cause());
                message.fail(500, "Database error.");
            }
        });
    }

    public void updateQuestion(Message<Object> message) {
    	logger.debug("Updating question...");

		JsonObject question = new JsonObject(message.body().toString());
		String questionId = question.getString(QUESTION_ID);

		JsonObject update = new JsonObject().put("$set", new JsonObject());
		for (String field : question.fieldNames()) {
			update.getJsonObject("$set").put(field, question.getValue(field));
		}

		JsonObject query = new JsonObject().put("_id", questionId);
		
		MongoUtil.getMongoClient().updateCollection("questions", query, update, reply -> {
			if (reply.succeeded()) {
				logger.debug("Matched : " + reply.result().getDocMatched());
				logger.debug("Updated : " + reply.result().getDocModified());
				message.reply(reply.result().toString());
			} else {
				logger.error("Error in database while updating question...", reply.cause());
				message.fail(500, "Database error.");
			}
		});
    }

    public void vote(Message<Object> message) {
    	logger.debug("voting on question...");

		JsonObject question = new JsonObject(message.body().toString());
		String questionId = question.getString(QUESTION_ID);
		int vote = question.getInteger("vote");

		JsonObject update = new JsonObject().put("$inc", new JsonObject().put("voteCount", vote));
		JsonObject query = new JsonObject().put("_id", questionId);
		
		MongoUtil.getMongoClient().updateCollection("questions", query, update, reply -> {
			if (reply.succeeded()) {
				logger.debug("Matched : " + reply.result().getDocMatched());
				logger.debug("Updated : " + reply.result().getDocModified());
				message.reply("");
			} else {
				logger.error("Database error while voting on question", reply.cause());
				message.fail(500, "Database error.");
			}
		});
    }
}
