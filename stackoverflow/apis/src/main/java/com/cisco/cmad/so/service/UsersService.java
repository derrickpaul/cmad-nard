package com.cisco.cmad.so.service;

import com.cisco.cmad.so.util.MongoUtil;


import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTOptions;
import io.vertx.ext.auth.jwt.impl.JWTAuthProviderImpl;



public class UsersService {

    private static final Logger logger = LoggerFactory.getLogger(UsersService.class.getName());

    public static final String USERS = "users";
    
    private JWTAuth provider ; 
    
    public void setProvider(JWTAuth provider) {
		this.provider = provider;
	}

	public void createUser(Message<Object> message) {


        JsonObject document = new JsonObject(message.body().toString());
        logger.debug("Creating a new user."+document.toString());
        String userName = document.getString("userName");
        
        //document.put("_id", userName);
        logger.debug("Creating a new user."+document.toString());
        //check if user already exists in DB.
        //insert DB Entry
        MongoUtil.getMongoClient().insert(USERS, document, reply -> {
            if (reply.succeeded()) {
                message.reply(reply.result());
            } else {            	
                logger.error("Error Creating user in Database", reply.cause());
                message.fail(500, "Database error.");
            }
        });
    }

    public void searchUsers(Message<Object> message) {

    }

    public void getUserById(Message<Object> message) {

    }

    public void updateUser(Message<Object> message) {

    }

    public void authenticate(Message<Object> message) {

    	logger.info("authenticate the user.");
        JsonObject document = new JsonObject(message.body().toString());
        String userName = document.getString("userName"); 
        String password = document.getString("password");
        JsonObject query = new JsonObject();
        query.put("userName", userName);
        
        logger.info("find the query."+query);
                


        
        MongoUtil.getMongoClient().findOne(USERS, query, null, reply -> {
            if (reply.succeeded()) {

				JsonObject dbRecord = reply.result();
				String dbPassword = dbRecord.getString("password");
				String token = "";

				if (dbPassword.equals(password)) {
					token = generateUserToken(dbRecord);					
					dbRecord.put("jwtToken", token);
				} 
        		message.reply(dbRecord.toString());	

            } else {
                logger.error("Error getting question by ID from database", reply.cause());
                message.fail(500, "Database error.");
            }
        });

    }
    
	private String generateUserToken(JsonObject dbRecord) {

		String userId = dbRecord.getString("userName");
		String firstName = dbRecord.getString("firstName");
		String lastName = dbRecord.getString("lastName");

		JsonObject userToken = new JsonObject().put("sub", userId).put("firstName", firstName);
		if (lastName != null)
			userToken.put("lastName", lastName);

		String token = provider.generateToken(userToken, new JWTOptions());
		logger.info("Token Generated:"+token);
		return token;
	}
}
