package com.cisco.cmad.so.util;

import com.cisco.cmad.so.service.QuestionsService;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.mongo.MongoClient;

public class MongoUtil {

	private static Logger logger = LoggerFactory.getLogger(MongoUtil.class.getName());

	private static MongoClient client = null;

	private MongoUtil() {
	}

	public static synchronized void initialize(Vertx vertx) {
		if (client != null)
			return;

		logger.info("Initializing mongo client");

		// TODO : Pick the configurations from JSON file.
		JsonObject config = new JsonObject();
		config.put("db_name", "cmad");
		config.put("connection_string", "mongodb://cmad-mongodb:27017");
		client = MongoClient.createShared(vertx, config);
		
		// Create indexes on the MongoDB instance.
        initializeIndices();
	}

	public static MongoClient getMongoClient() {
		return client;
	}

	public static boolean isInitialized() {
		return client != null;
	}
	
	private static void initializeIndices() {

        // Indexes for Questions collection.
        logger.info("Initializing indices for questions collection");
        JsonObject textSearchIndex = new JsonObject();
        textSearchIndex.put("text", "text");
        getMongoClient().createIndex(QuestionsService.QUESTIONS, textSearchIndex, result -> {
            if (result.failed()) {
                logger.error("Error created text search index on questions.", result.cause());
            }
        });
    }
}
