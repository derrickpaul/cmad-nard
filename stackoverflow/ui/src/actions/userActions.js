"use strict";

var Dispatcher = require('../dispatcher/appDispatcher');
var UserApi = require('../api/userApi');
var ActionTypes = require('../constants/actionTypes');

var UserActions = {
	createUser: function(user) {
		var newUser = UserApi.saveUser(user);

		//Hey dispatcher, go tell all the stores that an User was just created.
		Dispatcher.dispatch({
			actionType: ActionTypes.CREATE_USER,
			user: newUser
		});
	},

	updateUser: function(user) {
		var updatedUser = UserApi.saveUser(user);

		Dispatcher.dispatch({
			actionType: ActionTypes.UPDATE_USER,
			user: updatedUser
		});
	},

	deleteUser: function(userName) {
		UserApi.deleteUser(userName);

		Dispatcher.dispatch({
			actionType: ActionTypes.DELETE_USER,
			userName: userName
		});
	}
};

module.exports = UserActions;