"use strict";

//This file is mocking a web API by hitting hard coded data.
var users = require('./userData').users;
var _ = require('lodash');

//This would be performed on the server in a real app. Just stubbing in.
var _generateUserName = function(user) {
	return user.firstName.toLowerCase() + '-' + user.lastName.toLowerCase();
};

var _clone = function(item) {
	return JSON.parse(JSON.stringify(item)); //return cloned copy so that the item is passed by value instead of by reference
};

var UserApi = {
	getAllUsers: function() {
		return _clone(users); 
	},

	getUserByUserName: function(userName) {
		var user = _.find(users, {userName: userName});
		return _clone(user);
	},
	
	saveUser: function(user) {
		//pretend an ajax call to web api is made here
		console.log('Pretend this just saved the user to the DB via AJAX call...');
		
		if (user.userName) {
			var existingUserIndex = _.indexOf(users, _.find(users, {userName: user.userName})); 
			users.splice(existingUserIndex, 1, user);
		} else {
			//Just simulating creation here.
			//The server would generate userName for new user in a real app.
			//Cloning so copy returned is passed by value rather than by reference.
			user.userName = _generateUserName(user);
			users.push(user);
		}

		return _clone(user);
	},

	deleteUser: function(userName) {
		console.log('Pretend this just deleted the user from the DB via an AJAX call...');
		_.remove(users, { userName: userName});
	}
};

module.exports = UserApi;