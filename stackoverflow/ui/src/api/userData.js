module.exports = {
	users: 
	[
		{
			userName: 'nadevara', 
			firstName: 'Nandan', 
			lastName: 'Devarajulu',
            email: 'nadevara@cisco.com',
            password: 'iforgot'
		},	        
		{
            userName: 'derrick', 
			firstName: 'Derrick', 
			lastName: 'Paul',
            email: 'derrick@cisco.com',
            password: 'secret'
            
		},	
		{
			userName: 'arun', 
			firstName: 'Arun', 
			lastName: 'John',
            email: 'arun@cisco.com',
            password: 'confidential'            
		}
	]
};