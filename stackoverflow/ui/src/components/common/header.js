"use strict";

var React = require('react');
var Router = require('react-router');
var Link = Router.Link;

var Header = React.createClass({
	render: function() {
		return (
        <nav className="navbar navbar-inverse">
          <div className="container-fluid">
                <div class="navbar-header">
                  <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span className="sr-only">Toggle navigation</span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                  </button>
                  <a className="navbar-brand" href="#">
                     <Link to="app" className="navbar-brand">
                        <img src="images/heapoverflow-logo.png" />
                     </Link>
                  </a>
                </div>
             <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

              <ul className="nav navbar-nav">
                <li><Link to="app">Home</Link></li>
                <li><Link to="users">Users</Link></li>
                <li><Link to="about">About</Link></li>
              </ul>
               <ul className="nav navbar-nav navbar-right">
                <li><a href="#">Logout</a></li>
                <li className="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span className="caret"></span></a>
                      <ul className="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                      </ul>
                    </li>            
               </ul>
            
            </div>  
          </div> 
        </nav>
		);
	}
});

module.exports = Header;
