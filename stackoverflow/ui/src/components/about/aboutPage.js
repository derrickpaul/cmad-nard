"use strict";

var React = require('react');

var About = React.createClass({
	statics: {
		willTransitionTo: function(transition, params, query, callback) {
			if (!confirm('Are you sure you want to read a page that\'s this boring?')) {
				transition.about();
			} else {
				callback();
			}
		},
		
		willTransitionFrom: function(transition, component) {
			if (!confirm('Are you sure you want to leave a page that\'s this exciting?')) {
				transition.about();
			}
		}
	},
	render: function () {
		return (
			<div>
              <div>
				<h1>About</h1>
				<p>
					This is co-hort project:

				</p>
              </div>
              <div className="jumbotron">
                <div className="row">
                <div className="col-lg-12">
                    <h3>Team</h3>
                </div>
                <div className="col-lg-2">
                    
                    <img src="images/nadevara.jpg" className="img-circle" width="150" height="175" /> 
                    <p>Nandan Devarajulu</p>
                </div>
                <div className="col-lg-2">
                    
                    <img src="images/derpaul.jpg" className="img-circle" width="150" height="175" /> 
                    <p>Derrick Paul</p>
                </div>
                <div className="col-lg-2">
                    
                    <img src="images/arunjoh.jpg" className="img-circle" width="150" height="175" /> 
                    <p>Arun John</p>
                </div>            
                </div>
              </div>
              <div>
                    Technologies used:
                      <ul>
						<li>React</li>
						<li>React Router</li>
						<li>Flux</li>
						<li>Node</li>
						<li>Gulp</li>
						<li>Browserify</li>
						<li>Bootstrap</li>
					</ul>
              </div>
			</div>
		); 
	}
});

module.exports = About;