"use strict";

var React = require('react');
var Router = require('react-router');
var Link = Router.Link;
var UserActions = require('../../actions/UserActions');
var toastr = require('toastr');

var UserList = React.createClass({
	propTypes: {
		users: React.PropTypes.array.isRequired
	},

	deleteUser: function(userName, event) {
		event.preventDefault();
		UserActions.deleteUser(userName);
		toastr.success('User Deleted');
	},

	render: function() {
		var createUserRow = function(user) {
			return (
				<tr key={user.userName}>
					<td><a href="#" onClick={this.deleteUser.bind(this, user.userName)}>Delete</a></td>
					<td><Link to="manageUser" params={{userName: user.userName}}>{user.userName}</Link></td>
					<td>{user.firstName} {user.lastName}</td>
				</tr>
			);
		};

		return (
			<div>
				<table className="table">
					<thead>
						<th></th>
						<th>UserName</th>
						<th>Name</th>
					</thead>
					<tbody>
						{this.props.users.map(createUserRow, this)}
					</tbody>
				</table>
			</div>
		);
	}
});

module.exports = UserList;