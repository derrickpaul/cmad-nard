"use strict";

var React = require('react');
var Input = require('../common/textInput');

var UserForm = React.createClass({
	propTypes: {
		user:	React.PropTypes.object.isRequired,
		onSave:	React.PropTypes.func.isRequired,
		onChange: React.PropTypes.func.isRequired,
		errors: React.PropTypes.object
	},
    
    getPageLabel: function(user){
        return (user.userName) ? 'Edit user' : 'Create user';            
    },
    
	render: function() {
		return (
            <div className="row">
            <div className="col-md-2"/>
            <div className="col-md-4">            
            <form className="form-horizantal">
				<h3>{ this.getPageLabel(this.props.user)}</h3>
				<Input
					name="firstName"
					label="First Name"
					value={this.props.user.firstName}
					onChange={this.props.onChange}
					error={this.props.errors.firstName} 
                    placeholder="First name" />

				<Input
					name="lastName"
					label="Last Name"
					value={this.props.user.lastName}
					onChange={this.props.onChange}
					error={this.props.errors.lastName}
                    placeholder="Last name"/>
            
				<Input
					name="email"
					label="Email"
					value={this.props.user.email}
					onChange={this.props.onChange}
					error={this.props.errors.email}
                    placeholder="Email address"/>
            
				<Input
					name="password"
					label="password"
					value={this.props.user.password}
					onChange={this.props.onChange}
					error={this.props.errors.password} 
                    placeholder="your secret password please"/>            

				<input type="submit" value="Save" className="btn btn-default" onClick={this.props.onSave} />
                <input type="button" value="Cancel" className="btn btn-default" />
            </form>
            </div>                     
            <div className="col-md-6"/>
         </div>                         
		);
	}
});

module.exports = UserForm;