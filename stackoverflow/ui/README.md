# README #

This README document provides steps necessary to get the UI application up and running.

Step 1) Check out this project and navigate to this folder 'ui'
Step 2) type "npm install gulp@3.9.0"
Step 3) type "npm install"
Step 4) type "gulp", it must launch browser




Current Status:
----------------
Brings up basic UI.
Contains only Users page implementation
Users page
    : currently working with hardcoded data
    : fields are basic, looks are basic.

Next Steps
---------

    Users page
    : change it to work with REST URL.
    : update UI and fields.